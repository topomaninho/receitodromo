--
-- File generated with SQLiteStudio v3.1.1 on Sat Jun 30 18:47:08 2018
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: detalhes
CREATE TABLE detalhes (
    idIngrediente INTEGER REFERENCES ingredientes (id) ON DELETE NO ACTION
                                                       ON UPDATE NO ACTION
                          NOT NULL,
    idreceitas    INTEGER REFERENCES receita (id) ON DELETE NO ACTION
                                                  ON UPDATE NO ACTION
                          NOT NULL,
    quantidade    INTEGER NOT NULL,
    unidades      STRING
);

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         1,
                         1,
                         40,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         2,
                         1,
                         50,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         3,
                         1,
                         50,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         4,
                         1,
                         100,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         5,
                         1,
                         12,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         6,
                         1,
                         12,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         9,
                         1,
                         60,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         8,
                         1,
                         60,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         7,
                         1,
                         6,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         14,
                         1,
                         5,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         5,
                         2,
                         24,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         10,
                         2,
                         100,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         11,
                         2,
                         100,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         12,
                         2,
                         400,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         13,
                         2,
                         300,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         15,
                         2,
                         100,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         17,
                         2,
                         15,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         16,
                         2,
                         5,
                         'decilitros'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         22,
                         2,
                         59,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         10,
                         3,
                         80,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         18,
                         3,
                         2,
                         ''
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         19,
                         3,
                         300,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         20,
                         3,
                         200,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         12,
                         3,
                         200,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         5,
                         3,
                         24,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         17,
                         3,
                         15,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         21,
                         3,
                         500,
                         'gramas'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         16,
                         3,
                         1,
                         'litro'
                     );

INSERT INTO detalhes (
                         idIngrediente,
                         idreceitas,
                         quantidade,
                         unidades
                     )
                     VALUES (
                         23,
                         20,
                         200,
                         'gramas'
                     );


-- Table: ingredientes
CREATE TABLE ingredientes (
    id        INTEGER NOT NULL
                      PRIMARY KEY AUTOINCREMENT,
    nome      STRING  NOT NULL,
    calorias  INTEGER NOT NULL,
    descricao TEXT,
    preco     DECIMAL NOT NULL,
    foto      STRING
);

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             1,
                             'aipo',
                             6,
                             NULL,
                             13.95,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             2,
                             'cenoura',
                             20,
                             NULL,
                             0.8,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             3,
                             'beterraba',
                             18,
                             NULL,
                             1.39,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             4,
                             'alface',
                             15,
                             'Alface � uma hortense anual ou bienal, utilizada na alimenta��o humana desde cerca de 500 a.C..',
                             0.9,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             5,
                             'azeite',
                             884,
                             'Azeite � um produto alimentar extra�do da azeitona, o fruto da oliveira. plantado em pleno calor.',
                             3,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             6,
                             'vinagre',
                             18,
                             NULL,
                             3.27,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             7,
                             'mostarda',
                             66,
                             'A mostarda � um condimento � base de semente de mostarda, �gua e vinagre.',
                             2.99,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             8,
                             'feijao',
                             339,
                             NULL,
                             0.49,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             9,
                             'mirtilos',
                             57,
                             NULL,
                             6.89,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             10,
                             'cebola',
                             40,
                             'Cebola � o nome popular da planta cujo nome cient�fico � Allium cepa. ',
                             1.09,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             11,
                             'ervilha',
                             81,
                             NULL,
                             2.35,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             12,
                             'brocolo',
                             34,
                             NULL,
                             1.89,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             13,
                             'courgette',
                             20,
                             NULL,
                             3.87,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             14,
                             'mel',
                             304,
                             NULL,
                             5.79,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             15,
                             'espinafre',
                             100,
                             NULL,
                             2.28,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             16,
                             'agua',
                             0,
                             '�gua pot�vel � a �gua de qualidade suficiente para se beber e preparar alimentos.',
                             0.5,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             17,
                             'sal',
                             0,
                             'O sal de cozinha ou sal comum � um mineral formado principalmente por cloreto de s�dio.',
                             1.34,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             18,
                             'dente de alho',
                             10,
                             NULL,
                             0.6,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             19,
                             'batata',
                             77,
                             NULL,
                             0.7,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             20,
                             'abobora',
                             26,
                             NULL,
                             1.59,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             21,
                             'grao-de-bico',
                             364,
                             NULL,
                             2.89,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             22,
                             'coentros',
                             49,
                             'Coentro � uma planta glabra, da fam�lia Apiaceae, de flores r�seas ou alvas, pequenas e arom�ticas, cujo fruto � diaqu�nio e cuja folha, usada como tempero ou condimento, exala odor caracter�stico.',
                             2.49,
                             NULL
                         );

INSERT INTO ingredientes (
                             id,
                             nome,
                             calorias,
                             descricao,
                             preco,
                             foto
                         )
                         VALUES (
                             23,
                             'carne de vaca',
                             20,
                             NULL,
                             10,
                             NULL
                         );


-- Table: listaCompras
CREATE TABLE listaCompras (
    id   INTEGER PRIMARY KEY AUTOINCREMENT
                 NOT NULL,
    data DATE    NOT NULL,
    loja STRING
);


-- Table: receita
CREATE TABLE receita (
    id            INTEGER PRIMARY KEY AUTOINCREMENT
                          NOT NULL,
    nome          STRING  NOT NULL,
    idTipo        INTEGER REFERENCES tipoReceita (id) ON DELETE CASCADE
                                                      ON UPDATE CASCADE,
    nDoses        INTEGER NOT NULL,
    tempo         INTEGER,
    preparacao    STRING,
    foto          STRING,
    classificacao INTEGER
);

INSERT INTO receita (
                        id,
                        nome,
                        idTipo,
                        nDoses,
                        tempo,
                        preparacao,
                        foto,
                        classificacao
                    )
                    VALUES (
                        1,
                        'Salada Primavera',
                        1,
                        4,
                        15,
                        'Lave o talo de aipo, corte-o em l�minas finas e coloque-o na saladeira.
Pele as cenouras e a beterraba, corte as cenouras em tiras e a beterraba em rodelas e junte-as ao aipo.
Separe as folhas da alface, lave-as e escorra-as bem e coloque-as na saladeira. Reserve.
Junte o azeite, o vinagre, o mel e a mostarda num copo, misture bem com um garfo ou uma vara de arames e verta-o por cima dos legumes.
Salpique com os rebentos de feij�o e os mirtilos e sirva de imediato.',
                        'imagens/salada-primavera.jpg',
                        4
                    );

INSERT INTO receita (
                        id,
                        nome,
                        idTipo,
                        nDoses,
                        tempo,
                        preparacao,
                        foto,
                        classificacao
                    )
                    VALUES (
                        2,
                        'Creme de br�culos',
                        1,
                        6,
                        30,
                        'Deite o azeite e os legumes, com excep��o dos espinafres, numa panela, salpique com o sal e leve ao lume.
Deixe estufar tapado cerca de 13 minutos, em lume m�dio a forte, mexendo de vez em quando.
Adicione as folhas de espinafres e a �gua a ferver e deixe cozer mais 5 a 6 minutos.
Junte uma m�o-cheia de folhas de coentros � sopa e triture tudo com a varinha m�gica, ou num copo liquidificador, at� obter um creme.',
                        'imagens/Creme-de-broculos.jpg',
                        3
                    );

INSERT INTO receita (
                        id,
                        nome,
                        idTipo,
                        nDoses,
                        tempo,
                        preparacao,
                        foto,
                        classificacao
                    )
                    VALUES (
                        3,
                        'Sopa do bosque',
                        1,
                        6,
                        60,
                        'Descasque a cebola e os dentes de alho, pique-os finamente e deite numa panela.
Adicione as batatas e a ab�bora cortadas em cubos. Reserve.
Corte a ab�bora em fatias finas. Deite as aparas da ab�bora na panela.
Separe os ramos dos br�colos e lave-os em �gua abundante. Reserve a parte de cima dos ramos, pique os talos em peda�os pequenos e junte-os aos restantes legumes na panela.
Regue com o azeite, salpique com o sal e cozinhe em lume brando cerca de 15 minutos.
Escorra o gr�o, passe-o por �gua fria e coloque-o na panela. Adicione a �gua a ferver e deixe cozinhar mais 20 minutos.
Junte a ab�bora e os ramos de br�colos reservados numa ta�a de vidro, cubra com uma tampa pr�pria para microondas ou com pel�cula aderente e coza no microondas 5 minutos na pot�ncia m�xima.
Triture a sopa na panela com a varinha m�gica e distribua-a pelos pratos ou ta�as.
Decore com os ramos de br�colos e as figuras de ab�bora.',
                        'imagens/sopa-do-bosque.jpg',
                        5
                    );

INSERT INTO receita (
                        id,
                        nome,
                        idTipo,
                        nDoses,
                        tempo,
                        preparacao,
                        foto,
                        classificacao
                    )
                    VALUES (
                        20,
                        'Carne de vaca no forno',
                        2,
                        4,
                        60,
                        'Corte a carne em peda�os e leve ao forno a 200 graus durante 1 hora.',
                        'imagens/carne.jpg',
                        NULL
                    );


-- Table: tipoReceita
CREATE TABLE tipoReceita (
    id   INTEGER PRIMARY KEY AUTOINCREMENT
                 NOT NULL,
    nome STRING  NOT NULL
);

INSERT INTO tipoReceita (
                            id,
                            nome
                        )
                        VALUES (
                            1,
                            'Entrada'
                        );

INSERT INTO tipoReceita (
                            id,
                            nome
                        )
                        VALUES (
                            2,
                            'Carne'
                        );

INSERT INTO tipoReceita (
                            id,
                            nome
                        )
                        VALUES (
                            3,
                            'Peixe'
                        );

INSERT INTO tipoReceita (
                            id,
                            nome
                        )
                        VALUES (
                            4,
                            'Vegetariano'
                        );

INSERT INTO tipoReceita (
                            id,
                            nome
                        )
                        VALUES (
                            5,
                            'Massa'
                        );

INSERT INTO tipoReceita (
                            id,
                            nome
                        )
                        VALUES (
                            6,
                            'Sobremesa'
                        );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
