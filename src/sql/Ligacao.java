package sql;

import java.sql.*;

public class Ligacao {
    // JDBC driver name and database URL

    static final String JDBC_DRIVER = "org.sqlite.JDBC";
    static final String DB_URL = "jdbc:sqlite:Receitas.db";
    Connection conn = null;

    public static Connection dbConnector() {

        try {
            Class.forName(JDBC_DRIVER);
            Connection conn = DriverManager.getConnection(DB_URL);
            return conn;
        } catch (Exception e) {
            return null;
        }
    }
}
