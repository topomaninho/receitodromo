package sql;

import java.sql.Blob;
import java.sql.Time;

public class Ingrediente {

    private int id;
    private String nome;
    private int calorias;
    private String descricao;
    private float preco;
    private String foto;
    private int quantidade;
    private String unidades;

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getUnidades() {
        return unidades;
    }

    public void setUnidades(String unidades) {
        this.unidades = unidades;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCalorias() {
        return calorias;
    }

    public void setCalorias(int calorias) {
        this.calorias = calorias;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Ingrediente(int id, String nome, int calorias, String descricao, float preco, String foto, int quantidade, String unidades) {
        this.id = id;
        this.nome = nome;
        this.calorias = calorias;
        this.descricao = descricao;
        this.preco = preco;
        this.foto = foto;
        this.quantidade = quantidade;
        this.unidades = unidades;

    }

}
