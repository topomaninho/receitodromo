package sql;

import java.sql.Blob;
import java.sql.Time;

public class Receita {

    private int id;
    private String nome;
    private int idTipo;
    private int nDoses;
    private int tempo;
    private String preparacao;
    private String foto;
    private int classificacao;

    public Receita(int id, String nome, int idTipo, int nDoses, int tempo, String preparacao, String foto, int classificacao) {
        this.id = id;
        this.nome = nome;
        this.idTipo = idTipo;
        this.nDoses = nDoses;
        this.tempo = tempo;
        this.preparacao = preparacao;
        this.foto = foto;
        this.classificacao = classificacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getnDoses() {
        return nDoses;
    }

    public void setnDoses(int nDoses) {
        this.nDoses = nDoses;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    public String getPreparacao() {
        return preparacao;
    }

    public void setPreparacao(String preparacao) {
        this.preparacao = preparacao;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

}
