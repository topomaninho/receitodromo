package main;

import java.sql.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import sql.Receita;
import javax.swing.table.DefaultTableModel;
import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import sql.Ingrediente;
import my.testpic.PicUtil;

public class Receitodromo extends javax.swing.JFrame {

    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement pst = null;

    public Receitodromo() {
        initComponents();
        conn = sql.Ligacao.dbConnector();  // faz a ligacao a base de dados

        //sempre que o programa abre mostra o painel1
        jPanel1.setVisible(true);
        jPanel2.setVisible(false);
        jPanel3.setVisible(false);

        try {
            fillCombo();
        } catch (SQLException ex) {
            Logger.getLogger(Receitodromo.class.getName()).log(Level.SEVERE, null, ex);
        }
        findReceitas();
        tableReceitas.getSelectionModel().addListSelectionListener(new ListSelectionListener() { //listener para quando for escolhida uma receita ir procurar os seus ingredientes e preparacao
            public void valueChanged(ListSelectionEvent event) {
                int id = Integer.parseInt(tableReceitas.getValueAt(tableReceitas.getSelectedRow(), 0).toString());
                findIngredientes(id);
                mostraPreparacao(id);
            }
        }
        );

        tableReceitas2.getSelectionModel().addListSelectionListener(new ListSelectionListener() {// mesmo que o listener de cima mas para o explorador de receitas
            public void valueChanged(ListSelectionEvent event) {
                int id = Integer.parseInt(tableReceitas2.getValueAt(tableReceitas2.getSelectedRow(), 0).toString());
                findIngredientes2(id);
            }
        }
        );

    }

    public ArrayList<Receita> listaReceitas(String ValToSearch) { // devolve uma lista das receitas que contenham no nome o que o utilizador inseriu na caixa de texto
        ArrayList<Receita> listaReceitas = new ArrayList<Receita>();

        Statement st;
        ResultSet rs;

        try {
            st = conn.createStatement();
            String searchQuery = "SELECT `receita`.`id`, `receita`.`nome`, `receita`.`idTipo`, `receita`.`nDoses`, `receita`.`preparacao`, `receita`.`tempo`, `receita`.`foto`, `receita`.`classificacao` FROM `receita` WHERE `receita`.`nome` LIKE '%" + ValToSearch + "%'";
            rs = st.executeQuery(searchQuery);

            Receita receita;

            while (rs.next()) {
                receita = new Receita(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("idTipo"),
                        rs.getInt("nDoses"),
                        rs.getInt("tempo"),
                        rs.getString("preparacao"),
                        rs.getString("foto"),
                        rs.getInt("classificacao")
                );
                listaReceitas.add(receita);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return listaReceitas;
    }

    // devolve uma lista de receitas semelhante a ListaReceitas mas que contenham os ingredientes selecionados pelo utilizador no explorador de receitas
    public ArrayList<Receita> listaReceitas2(int ing1, int ing2, int ing3, int ing4) {
        ArrayList<Receita> listaReceitas2 = new ArrayList<Receita>();

        Statement st;
        ResultSet rs;

        try {
            String searchQuery = "SELECT `receita`.`id`, `receita`.`nome`, `receita`.`idTipo`, `receita`.`nDoses`, `receita`.`preparacao`, `receita`.`tempo`, `receita`.`foto`, `receita`.`classificacao` FROM `receita` WHERE `receita`.`id` IN(SELECT `detalhes`.`idreceitas` FROM `detalhes` WHERE ";

            if (ing1 > 0) {
                searchQuery += "`detalhes`.`idIngrediente` = " + ing1;
            }
            if (ing1 > 0 && ing2 > 0) {
                searchQuery += " OR `detalhes`.`idIngrediente` = " + ing2;
            } else if (ing2 > 0) {
                searchQuery += " `detalhes`.`idIngrediente` = " + ing2;
            }
            if (ing1 > 0 || ing2 > 0 && ing3 > 0) {
                searchQuery += " OR `detalhes`.`idIngrediente` = " + ing3;
            } else if (ing3 > 0) {
                searchQuery += " `detalhes`.`idIngrediente` = " + ing3;
            }
            if (ing1 > 0 || ing2 > 0 || ing3 > 0 && ing4 > 0) {
                searchQuery += " OR `detalhes`.`idIngrediente` = " + ing4;
            } else if (ing4 > 0) {
                searchQuery += " or `detalhes`.`idIngrediente` = " + ing4;
            }
            searchQuery += ");";

            st = conn.createStatement();
            rs = st.executeQuery(searchQuery);

            Receita receita;

            while (rs.next()) {
                receita = new Receita(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("idTipo"),
                        rs.getInt("nDoses"),
                        rs.getInt("tempo"),
                        rs.getString("preparacao"),
                        rs.getString("foto"),
                        rs.getInt("classificacao")
                );
                listaReceitas2.add(receita);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return listaReceitas2;
    }

    //devolve uma lista de ingredientes da receita que o utilizador selecionou na tabela de receitas
    public ArrayList<Ingrediente> listaIngredientes(int ValToSearch) {
        ArrayList<Ingrediente> listaIngredientes = new ArrayList<Ingrediente>();

        Statement st;
        ResultSet rs;

        try {
            st = conn.createStatement();
            String searchQuery = "SELECT `ingredientes`.`id`, `ingredientes`.`nome`, `ingredientes`.`calorias`, `ingredientes`.`descricao`, `ingredientes`.`preco`, `ingredientes`.`foto`, `detalhes`.`quantidade`, `detalhes`.`unidades` FROM `ingredientes`, `detalhes` WHERE `detalhes`.`idIngrediente` = `ingredientes`.`id` AND `detalhes`.`idreceitas` = " + ValToSearch + "";
            rs = st.executeQuery(searchQuery);

            Ingrediente ingrediente;

            while (rs.next()) {
                ingrediente = new Ingrediente(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("calorias"),
                        rs.getString("descricao"),
                        rs.getFloat("preco"),
                        rs.getString("foto"),
                        rs.getInt("quantidade"),
                        rs.getString("unidades")
                );
                listaIngredientes.add(ingrediente);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return listaIngredientes;
    }

    //pede uma lista de receitas a funcao anterior e mostra essa lista na tabela
    public void findReceitas() {

        tableReceitas.clearSelection();

        ArrayList<Receita> receita = listaReceitas(receitasSearch.getText());

        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new Object[]{"ID", "Nome", "Tipo", "Doses", "Tempo (minutos)", "Classificação"});
        Object[] row = new Object[6];

        int idTipo;
        for (int i = 0; i < receita.size(); i++) {
            row[0] = receita.get(i).getId();
            row[1] = receita.get(i).getNome();
            idTipo = receita.get(i).getIdTipo();
            switch (idTipo) {
                case 1:
                    row[2] = "Entrada";
                    break;
                case 2:
                    row[2] = "Carne";
                    break;
                case 3:
                    row[2] = "Peixe";
                    break;
                case 4:
                    row[2] = "Vegetariano";
                    break;
                case 5:
                    row[2] = "Massa";
                    break;
                case 6:
                    row[2] = "Sobremesa";
                    break;
            }
            row[3] = receita.get(i).getnDoses();
            txtPreparacao.setText(receita.get(i).getPreparacao());
            row[4] = receita.get(i).getTempo();

            lblFoto.setIcon(new ImageIcon(receita.get(i).getFoto()));

            row[5] = receita.get(i).getClassificacao();
            model.addRow(row);
        }
        tableReceitas.setModel(model);
        tableReceitas.repaint();

    }

    //insere a preparacao da receita selecionada na tabelaReceitas na caixa de texto
    public void mostraPreparacao(int id) {

        Statement st;
        ResultSet rs;

        try {
            st = conn.createStatement();
            String searchQuery = "SELECT `receita`.`id`, `receita`.`nome`, `receita`.`idTipo`, `receita`.`nDoses`, `receita`.`preparacao`, `receita`.`tempo`, `receita`.`foto`, `receita`.`classificacao` FROM `receita` WHERE `receita`.`id` = " + id + "";
            rs = st.executeQuery(searchQuery);

            while (rs.next()) {
                if (rs.getInt("id") == id) {
                    txtPreparacao.setText(rs.getString("preparacao"));
                    lblFoto.setIcon(new ImageIcon(rs.getString("foto")));
                }

            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    //recebe a lista de ingredientes da listaIngredientes e mostra os ingredientes listados na tabelaIngredientes
    //tambem calcula o total de calorias da receita selecionada
    public void findIngredientes(int id) {
        ArrayList<Ingrediente> ingrediente = listaIngredientes(id);
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new Object[]{"ID", "Nome", "Calorias", "Preço", "Quantidade"});
        Object[] row = new Object[5];
        int calorias = 0;

        for (int i = 0; i < ingrediente.size(); i++) {
            row[0] = ingrediente.get(i).getId();
            row[1] = ingrediente.get(i).getNome();
            row[2] = ingrediente.get(i).getCalorias();
            row[3] = ingrediente.get(i).getPreco() + "€";
            row[4] = "" + ingrediente.get(i).getQuantidade() + " " + ingrediente.get(i).getUnidades();
            model.addRow(row);
            calorias += ingrediente.get(i).getCalorias();
        }
        tableIngredientes.setModel(model);
        lblCalorias.setText("" + calorias);
    }

    //como a funcao anterior mostra os ingredientes mas das receitas do explorador de receitas
    public void findIngredientes2(int id) {
        ArrayList<Ingrediente> ingrediente = listaIngredientes(id);
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new Object[]{"ID", "Nome", "Calorias", "Preço", "Quantidade"});
        Object[] row = new Object[5];
        int calorias = 0;

        for (int i = 0; i < ingrediente.size(); i++) {
            row[0] = ingrediente.get(i).getId();
            row[1] = ingrediente.get(i).getNome();
            row[2] = ingrediente.get(i).getCalorias();
            row[3] = ingrediente.get(i).getPreco() + "€";
            row[4] = "" + ingrediente.get(i).getQuantidade() + " " + ingrediente.get(i).getUnidades();
            model.addRow(row);
            calorias += ingrediente.get(i).getCalorias();
        }
        tableIngredientes2.setModel(model);
        lblCalorias2.setText("" + calorias);

    }

    //guarda na base de dados os dados de uma nova receita inseridos pelo utilizador no painel de nova receita
    public void novaReceita() {
        ArrayList<Receita> receita = listaReceitas("");
        int idReceita = receita.get(receita.size() - 1).getId() + 1;

        String nome = "";
        int idTipo = 0;
        int nDoses = 0;
        int tempo = 0;
        int classificacao;
        String preparacao;
        String foto;

        nome = txtNome.getText();

        if (rb1.isSelected()) {
            idTipo = 1;
        } else if (rb2.isSelected()) {
            idTipo = 2;
        } else if (rb3.isSelected()) {
            idTipo = 3;
        } else if (rb4.isSelected()) {
            idTipo = 4;
        } else if (rb5.isSelected()) {
            idTipo = 5;
        } else if (rb6.isSelected()) {
            idTipo = 6;
        }

        nDoses = (Integer) spinDoses.getValue();
        tempo = (Integer) spinTempo.getValue();
        classificacao = (Integer) spinClassificacao.getValue();
        preparacao = txtNovaPreparacao.getText();
        foto = lblImagem.getIcon().toString();

        int[] idIng = new int[12];
        String[] ing = new String[12];
        int[] qtd = new int[12];
        String[] uni = new String[12];

        idIng[0] = cb1.getSelectedIndex();
        idIng[1] = cb2.getSelectedIndex();
        idIng[2] = cb3.getSelectedIndex();
        idIng[3] = cb4.getSelectedIndex();
        idIng[4] = cb5.getSelectedIndex();
        idIng[5] = cb6.getSelectedIndex();
        idIng[6] = cb7.getSelectedIndex();
        idIng[7] = cb8.getSelectedIndex();
        idIng[8] = cb9.getSelectedIndex();
        idIng[9] = cb10.getSelectedIndex();
        idIng[10] = cb11.getSelectedIndex();
        idIng[11] = cb12.getSelectedIndex();

        ing[0] = cb1.getSelectedItem().toString();
        ing[1] = cb2.getSelectedItem().toString();
        ing[2] = cb3.getSelectedItem().toString();
        ing[3] = cb4.getSelectedItem().toString();
        ing[4] = cb5.getSelectedItem().toString();
        ing[5] = cb6.getSelectedItem().toString();
        ing[6] = cb7.getSelectedItem().toString();
        ing[7] = cb8.getSelectedItem().toString();
        ing[8] = cb9.getSelectedItem().toString();
        ing[9] = cb10.getSelectedItem().toString();
        ing[10] = cb11.getSelectedItem().toString();
        ing[11] = cb12.getSelectedItem().toString();

        qtd[0] = (Integer) spinQtd1.getValue();
        qtd[1] = (Integer) spinQtd2.getValue();
        qtd[2] = (Integer) spinQtd3.getValue();
        qtd[3] = (Integer) spinQtd4.getValue();
        qtd[4] = (Integer) spinQtd5.getValue();
        qtd[5] = (Integer) spinQtd6.getValue();
        qtd[6] = (Integer) spinQtd7.getValue();
        qtd[7] = (Integer) spinQtd8.getValue();
        qtd[8] = (Integer) spinQtd9.getValue();
        qtd[9] = (Integer) spinQtd10.getValue();
        qtd[10] = (Integer) spinQtd11.getValue();
        qtd[11] = (Integer) spinQtd12.getValue();

        uni[0] = txtQtd1.getText();
        uni[1] = txtQtd2.getText();
        uni[2] = txtQtd3.getText();
        uni[3] = txtQtd4.getText();
        uni[4] = txtQtd5.getText();
        uni[5] = txtQtd6.getText();
        uni[6] = txtQtd7.getText();
        uni[7] = txtQtd8.getText();
        uni[8] = txtQtd9.getText();
        uni[9] = txtQtd10.getText();
        uni[10] = txtQtd11.getText();
        uni[11] = txtQtd12.getText();

        if (!nome.equals("") && idTipo != 0 && nDoses != 0 && tempo != 0 && classificacao != 0 && !preparacao.equals("") && !foto.equals("")) {
            //guardar na bd
            Statement st;

            try {
                st = conn.createStatement();
                String sql = "INSERT INTO `receita` (`id`, `nome`, `nDoses`,`idTipo`, `tempo`, `preparacao`,  `foto`, `classificacao`) VALUES (NULL, '" + nome + "', '" + idTipo + "', '" + nDoses + "', '" + tempo + "', '" + preparacao + "', '" + foto + "', '" + classificacao + "')";
                st.executeUpdate(sql);

                for (int i = 0; i < 11; i++) {
                    if (!ing[i].equals("nenhum")) {
                        sql = "INSERT INTO `detalhes` (`idIngrediente`, `idReceitas`, `quantidade`,`unidades`) VALUES (" + idIng[i] + ", " + idReceita + ", " + qtd[i] + ", '" + uni[i] + "')";
                        st.executeUpdate(sql);

                    }
                }

                JOptionPane.showMessageDialog(null,
                        "A receita foi guardada com sucesso!",
                        "Mensagem",
                        JOptionPane.PLAIN_MESSAGE);

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

        } else {
            JOptionPane.showMessageDialog(null,
                    "Por favor insira todas as informações antes de guardar.",
                    "Erro",
                    JOptionPane.PLAIN_MESSAGE);
        }

    }

    //preenche as comboBoxes no programa inteiro com a lista de ingredientes existentes na base de dados
    public void fillCombo() throws SQLException {
        Statement st;
        ResultSet rs;
        try {
            st = conn.createStatement();
            String searchQuery = "SELECT * FROM `ingredientes`";
            rs = st.executeQuery(searchQuery);

            while (rs.next()) {
                cb1.addItem(rs.getString("nome"));
                cb2.addItem(rs.getString("nome"));
                cb3.addItem(rs.getString("nome"));
                cb4.addItem(rs.getString("nome"));
                cb5.addItem(rs.getString("nome"));
                cb6.addItem(rs.getString("nome"));
                cb7.addItem(rs.getString("nome"));
                cb8.addItem(rs.getString("nome"));
                cb9.addItem(rs.getString("nome"));
                cb10.addItem(rs.getString("nome"));
                cb11.addItem(rs.getString("nome"));
                cb12.addItem(rs.getString("nome"));
                cbIng1.addItem(rs.getString("nome"));
                cbIng2.addItem(rs.getString("nome"));
                cbIng3.addItem(rs.getString("nome"));
                cbIng4.addItem(rs.getString("nome"));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    //recebe a lista de receitas da listaReceitas2 e mostra as receitas na tabela do explorador de receitas
    public void findReceitas2() {

        int ing1 = cbIng1.getSelectedIndex();
        int ing2 = cbIng2.getSelectedIndex();
        int ing3 = cbIng3.getSelectedIndex();
        int ing4 = cbIng4.getSelectedIndex();

        tableReceitas2.clearSelection();

        if (ing1 > 0 || ing2 > 0 || ing3 > 0 || ing4 > 0) {
            ArrayList<Receita> receita = listaReceitas2(ing1, ing2, ing3, ing4);

            DefaultTableModel model = new DefaultTableModel();
            model.setColumnIdentifiers(new Object[]{"ID", "Nome", "Tipo", "Doses", "Tempo (minutos)", "Classificação"});
            Object[] row = new Object[6];

            int idTipo;
            for (int i = 0; i < receita.size(); i++) {
                row[0] = receita.get(i).getId();
                row[1] = receita.get(i).getNome();
                idTipo = receita.get(i).getIdTipo();
                switch (idTipo) {
                    case 1:
                        row[2] = "Entrada";
                        break;
                    case 2:
                        row[2] = "Carne";
                        break;
                    case 3:
                        row[2] = "Peixe";
                        break;
                    case 4:
                        row[2] = "Vegetariano";
                        break;
                    case 5:
                        row[2] = "Massa";
                        break;
                    case 6:
                        row[2] = "Sobremesa";
                        break;
                }
                row[3] = receita.get(i).getnDoses();
                row[4] = receita.get(i).getTempo();
                row[5] = receita.get(i).getClassificacao();
                model.addRow(row);
            }
            tableReceitas2.setModel(model);
            tableReceitas2.repaint();
        } else {
            JOptionPane.showMessageDialog(null,
                    "Por favor insira pelo menos 1 ingrediente.",
                    "Mensagem",
                    JOptionPane.PLAIN_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoTipo = new javax.swing.ButtonGroup();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        receitasSearch = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableReceitas = new javax.swing.JTable();
        btnProc = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableIngredientes = new javax.swing.JTable();
        lblFoto = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtPreparacao = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lblCalorias = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        txtNome = new javax.swing.JTextField();
        rb1 = new javax.swing.JRadioButton();
        rb2 = new javax.swing.JRadioButton();
        rb3 = new javax.swing.JRadioButton();
        rb4 = new javax.swing.JRadioButton();
        rb5 = new javax.swing.JRadioButton();
        rb6 = new javax.swing.JRadioButton();
        spinDoses = new javax.swing.JSpinner();
        spinTempo = new javax.swing.JSpinner();
        spinClassificacao = new javax.swing.JSpinner();
        btnImagem = new javax.swing.JButton();
        lblImagem = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtNovaPreparacao = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        spinQtd1 = new javax.swing.JSpinner();
        spinQtd2 = new javax.swing.JSpinner();
        spinQtd3 = new javax.swing.JSpinner();
        spinQtd4 = new javax.swing.JSpinner();
        spinQtd5 = new javax.swing.JSpinner();
        spinQtd6 = new javax.swing.JSpinner();
        spinQtd7 = new javax.swing.JSpinner();
        spinQtd8 = new javax.swing.JSpinner();
        spinQtd9 = new javax.swing.JSpinner();
        spinQtd10 = new javax.swing.JSpinner();
        spinQtd11 = new javax.swing.JSpinner();
        spinQtd12 = new javax.swing.JSpinner();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtQtd1 = new javax.swing.JTextField();
        txtQtd2 = new javax.swing.JTextField();
        txtQtd3 = new javax.swing.JTextField();
        txtQtd4 = new javax.swing.JTextField();
        txtQtd5 = new javax.swing.JTextField();
        txtQtd6 = new javax.swing.JTextField();
        txtQtd7 = new javax.swing.JTextField();
        txtQtd8 = new javax.swing.JTextField();
        txtQtd9 = new javax.swing.JTextField();
        txtQtd10 = new javax.swing.JTextField();
        txtQtd11 = new javax.swing.JTextField();
        txtQtd12 = new javax.swing.JTextField();
        cb1 = new javax.swing.JComboBox<>();
        cb2 = new javax.swing.JComboBox<>();
        cb3 = new javax.swing.JComboBox<>();
        cb4 = new javax.swing.JComboBox<>();
        cb5 = new javax.swing.JComboBox<>();
        cb6 = new javax.swing.JComboBox<>();
        cb7 = new javax.swing.JComboBox<>();
        cb8 = new javax.swing.JComboBox<>();
        cb9 = new javax.swing.JComboBox<>();
        cb10 = new javax.swing.JComboBox<>();
        cb11 = new javax.swing.JComboBox<>();
        cb12 = new javax.swing.JComboBox<>();
        jLabel27 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        cbIng1 = new javax.swing.JComboBox<>();
        cbIng2 = new javax.swing.JComboBox<>();
        cbIng3 = new javax.swing.JComboBox<>();
        cbIng4 = new javax.swing.JComboBox<>();
        jButton4 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        tableReceitas2 = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        tableIngredientes2 = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lblCalorias2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Receitodromo");

        jButton1.setText("Nova Receita");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Explorador de Receitas");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Lista de Receitas");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });

        receitasSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receitasSearchActionPerformed(evt);
            }
        });

        tableReceitas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6"
            }
        ));
        jScrollPane1.setViewportView(tableReceitas);

        btnProc.setText("Procurar");
        btnProc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcActionPerformed(evt);
            }
        });

        tableIngredientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6"
            }
        ));
        jScrollPane2.setViewportView(tableIngredientes);

        txtPreparacao.setEditable(false);
        txtPreparacao.setColumns(20);
        txtPreparacao.setRows(5);
        txtPreparacao.setMaximumSize(new java.awt.Dimension(660, 192));
        txtPreparacao.setMinimumSize(new java.awt.Dimension(660, 192));
        jScrollPane4.setViewportView(txtPreparacao);

        jLabel1.setText("Ingredientes");

        jLabel2.setText("Receitas");

        jLabel10.setText("Preparação:");

        jLabel14.setText("Total de calorias:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(199, 199, 199)
                        .addComponent(receitasSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProc))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(385, 385, 385)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(123, 123, 123)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCalorias, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 170, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(receitasSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnProc))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14))
                    .addComponent(lblCalorias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        grupoTipo.add(rb1);
        rb1.setText("Entrada");
        rb1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rb1ActionPerformed(evt);
            }
        });

        grupoTipo.add(rb2);
        rb2.setText("Carne");

        grupoTipo.add(rb3);
        rb3.setText("Peixe");

        grupoTipo.add(rb4);
        rb4.setText("Vegetariano");

        grupoTipo.add(rb5);
        rb5.setText("Massa");

        grupoTipo.add(rb6);
        rb6.setText("Sobremesa");

        btnImagem.setText("Carregar Imagem");
        btnImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImagemActionPerformed(evt);
            }
        });

        txtNovaPreparacao.setColumns(20);
        txtNovaPreparacao.setRows(5);
        jScrollPane3.setViewportView(txtNovaPreparacao);

        jLabel3.setText("Nome:");

        jLabel4.setText("Tipo:");

        jLabel5.setText("Número de doses:");

        jLabel6.setText("Tempo de preparação:");

        jLabel7.setText("Classificação:");

        jLabel8.setText("Foto:");

        jLabel9.setText("Preparação:");

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jLabel11.setText("(0 a 5)");

        jLabel12.setText("(em minutos)");

        jLabel25.setText("Quantidade");

        jLabel26.setText("Unidades");

        txtQtd1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd1ActionPerformed(evt);
            }
        });

        txtQtd2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd2ActionPerformed(evt);
            }
        });

        txtQtd3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd3ActionPerformed(evt);
            }
        });

        txtQtd4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd4ActionPerformed(evt);
            }
        });

        txtQtd5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd5ActionPerformed(evt);
            }
        });

        txtQtd6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd6ActionPerformed(evt);
            }
        });

        txtQtd7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd7ActionPerformed(evt);
            }
        });

        txtQtd8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd8ActionPerformed(evt);
            }
        });

        txtQtd9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd9ActionPerformed(evt);
            }
        });

        txtQtd10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd10ActionPerformed(evt);
            }
        });

        txtQtd11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd11ActionPerformed(evt);
            }
        });

        txtQtd12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtd12ActionPerformed(evt);
            }
        });

        cb1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb6.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb7.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb8.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb9.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb10.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb11.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cb12.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        jLabel27.setText("Ingredientes");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                                        .addGap(54, 54, 54)
                                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)))
                                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                                        .addContainerGap()
                                                        .addComponent(jLabel6))
                                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                                        .addContainerGap()
                                                        .addComponent(jLabel5)))
                                                .addGap(23, 23, 23)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(rb5)
                                                    .addComponent(rb4)
                                                    .addComponent(rb1)
                                                    .addComponent(rb6)
                                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                .addComponent(spinTempo, javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(spinDoses, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addComponent(spinClassificacao, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGap(18, 18, 18)
                                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel12)
                                                            .addComponent(jLabel11)))
                                                    .addComponent(rb3)
                                                    .addComponent(rb2))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(lblImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(btnImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                                        .addComponent(jLabel8)
                                                        .addGap(80, 80, 80))))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(btnGuardar)
                                                .addGap(134, 134, 134)))
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(34, 34, 34)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(cb1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb3, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb4, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb5, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb6, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb7, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb8, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cb9, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addComponent(cb12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cb11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(cb10, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addGap(69, 69, 69)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(spinQtd7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd8, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd9, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd10, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd11, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd12, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtQtd7, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                                            .addComponent(txtQtd8)
                                            .addComponent(txtQtd9)
                                            .addComponent(txtQtd10)
                                            .addComponent(txtQtd11, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txtQtd12)))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(spinQtd4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd6, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(txtQtd4))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txtQtd5, javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(txtQtd6)))))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(spinQtd3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spinQtd2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtQtd2)
                                            .addComponent(txtQtd3))))
                                .addGap(46, 46, 46))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(spinQtd1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel25))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(txtQtd1)
                                        .addGap(46, 46, 46))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(81, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(jLabel26)
                            .addComponent(jLabel27))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(spinQtd1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQtd1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(spinQtd2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQtd2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(spinQtd3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQtd3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtQtd5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtQtd6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txtQtd4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtQtd8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtQtd9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txtQtd7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtQtd11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinQtd12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtQtd12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cb12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txtQtd10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3)
                                    .addComponent(btnImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rb1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rb2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(rb3)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rb4)
                                .addGap(2, 2, 2)
                                .addComponent(rb5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rb6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinDoses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addGap(8, 8, 8)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinTempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel6))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(spinClassificacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel7)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(36, 36, 36)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(75, 75, 75)
                        .addComponent(btnGuardar)
                        .addGap(12, 12, 12)))
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        cbIng1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cbIng2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        cbIng3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));
        cbIng3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbIng3ActionPerformed(evt);
            }
        });

        cbIng4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nenhum" }));

        jButton4.setText("Procurar Receitas");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        tableReceitas2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6"
            }
        ));
        jScrollPane5.setViewportView(tableReceitas2);

        tableIngredientes2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6"
            }
        ));
        jScrollPane6.setViewportView(tableIngredientes2);

        jLabel13.setText("Escolha ate 4 ingredientes e clique no botão para obter receitas os utilizem.");

        jLabel15.setText("Receitas:");

        jLabel16.setText("Ingredientes:");

        jLabel17.setText("Total de calorias:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 883, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane6))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(274, 274, 274)
                                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 287, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(cbIng1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(73, 73, 73)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel13)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(cbIng2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(cbIng3, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(9, 9, 9)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbIng4, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblCalorias2, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addGap(38, 38, 38)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbIng1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbIng2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbIng3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbIng4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel17)
                    .addComponent(lblCalorias2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                .addGap(14, 14, 14)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 32, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 113, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnProcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcActionPerformed
        // TODO add your handling code here:
        findReceitas();
    }//GEN-LAST:event_btnProcActionPerformed

    private void receitasSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_receitasSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_receitasSearchActionPerformed

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        // TODO add your handling code here:
        jPanel1.setVisible(true);
        jPanel2.setVisible(false);
        jPanel3.setVisible(false);
    }//GEN-LAST:event_jButton3MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jPanel1.setVisible(false);
        jPanel2.setVisible(true);
        jPanel3.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        jPanel1.setVisible(false);
        jPanel2.setVisible(false);
        jPanel3.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void rb1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rb1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rb1ActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        novaReceita();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtQtd1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd1ActionPerformed

    private void txtQtd2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd2ActionPerformed

    private void txtQtd3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd3ActionPerformed

    private void txtQtd4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd4ActionPerformed

    private void txtQtd5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd5ActionPerformed

    private void txtQtd6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd6ActionPerformed

    private void txtQtd7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd7ActionPerformed

    private void txtQtd8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd8ActionPerformed

    private void txtQtd9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd9ActionPerformed

    private void txtQtd10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd10ActionPerformed

    private void txtQtd11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd11ActionPerformed

    private void txtQtd12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtd12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQtd12ActionPerformed

    private void cbIng3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbIng3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbIng3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        findReceitas2();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImagemActionPerformed
        // TODO add your handling code here:
        lblImagem.setIcon(PicUtil.getImage());
    }//GEN-LAST:event_btnImagemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Receitodromo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Receitodromo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Receitodromo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Receitodromo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Receitodromo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnImagem;
    private javax.swing.JButton btnProc;
    private javax.swing.JComboBox<String> cb1;
    private javax.swing.JComboBox<String> cb10;
    private javax.swing.JComboBox<String> cb11;
    private javax.swing.JComboBox<String> cb12;
    private javax.swing.JComboBox<String> cb2;
    private javax.swing.JComboBox<String> cb3;
    private javax.swing.JComboBox<String> cb4;
    private javax.swing.JComboBox<String> cb5;
    private javax.swing.JComboBox<String> cb6;
    private javax.swing.JComboBox<String> cb7;
    private javax.swing.JComboBox<String> cb8;
    private javax.swing.JComboBox<String> cb9;
    private javax.swing.JComboBox<String> cbIng1;
    private javax.swing.JComboBox<String> cbIng2;
    private javax.swing.JComboBox<String> cbIng3;
    private javax.swing.JComboBox<String> cbIng4;
    private javax.swing.ButtonGroup grupoTipo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel lblCalorias;
    private javax.swing.JLabel lblCalorias2;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JLabel lblImagem;
    private javax.swing.JRadioButton rb1;
    private javax.swing.JRadioButton rb2;
    private javax.swing.JRadioButton rb3;
    private javax.swing.JRadioButton rb4;
    private javax.swing.JRadioButton rb5;
    private javax.swing.JRadioButton rb6;
    private javax.swing.JTextField receitasSearch;
    private javax.swing.JSpinner spinClassificacao;
    private javax.swing.JSpinner spinDoses;
    private javax.swing.JSpinner spinQtd1;
    private javax.swing.JSpinner spinQtd10;
    private javax.swing.JSpinner spinQtd11;
    private javax.swing.JSpinner spinQtd12;
    private javax.swing.JSpinner spinQtd2;
    private javax.swing.JSpinner spinQtd3;
    private javax.swing.JSpinner spinQtd4;
    private javax.swing.JSpinner spinQtd5;
    private javax.swing.JSpinner spinQtd6;
    private javax.swing.JSpinner spinQtd7;
    private javax.swing.JSpinner spinQtd8;
    private javax.swing.JSpinner spinQtd9;
    private javax.swing.JSpinner spinTempo;
    private javax.swing.JTable tableIngredientes;
    private javax.swing.JTable tableIngredientes2;
    private javax.swing.JTable tableReceitas;
    private javax.swing.JTable tableReceitas2;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextArea txtNovaPreparacao;
    private javax.swing.JTextArea txtPreparacao;
    private javax.swing.JTextField txtQtd1;
    private javax.swing.JTextField txtQtd10;
    private javax.swing.JTextField txtQtd11;
    private javax.swing.JTextField txtQtd12;
    private javax.swing.JTextField txtQtd2;
    private javax.swing.JTextField txtQtd3;
    private javax.swing.JTextField txtQtd4;
    private javax.swing.JTextField txtQtd5;
    private javax.swing.JTextField txtQtd6;
    private javax.swing.JTextField txtQtd7;
    private javax.swing.JTextField txtQtd8;
    private javax.swing.JTextField txtQtd9;
    // End of variables declaration//GEN-END:variables
}
